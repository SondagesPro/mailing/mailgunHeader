<?php
/**
 * Add Mailgun var
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2022 Denis Chenu <http://www.sondages.pro>
 * @copyright 2017-2022 Dialogs <http://www.dialogs.ca>
 * @license AGPL
 * @version 1.0.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class mailgunHeader extends PluginBase
{

    protected $storage = 'DbStorage';

    static protected $name = 'mailgunHeader';
    static protected $description = 'Allow to add mailgun variables in SMTP email.';

    /* @var array[] aBaseReplacements by language*/
    private $aBaseReplacements = array();

    protected $settings = array(
        'XMailgunTags' => array(
            'type' => 'text',
            'label' => 'X-Mailgun-Tag',
            'help' => 'One tag by line, can use Expression Manager with token variable',
        ),
        'XMailgunVariables' => array(
            'type' => 'text',
            'label' => 'X-Mailgun-Variables',
            'help' => 'One tag by line, key and value separated by a <code>:</code>. If there are no <code>:</code> : only value is get in final json.',
        )
    );

    /**
     * Init to token
     */
    public function init()
    {
        $this->subscribe("beforeTokenEmail");
    }

    public function beforeTokenEmail()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $mailEvent = $this->event;
        $mailer = $mailEvent->get('mailer');
        $type = $mailEvent->get("type");
        $surveyId = $mailEvent->get('survey');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aToken = $mailEvent->get('token');
        $oToken = Token::model($surveyId)->findByPk($aToken['tid']);

        $rawSubject = $mailer->rawSubject;
        $rawBody = $mailer->rawBody;
        $language = $mailer->mailLanguage;


         /* Fix TOKEN:PID and TOKEN:PARTICIPANT_ID if needed */
        if ($type == 'confirm') {
            $mailer->aReplacements["TOKEN:PID"] = $oToken->participant_id;
        } else {
            $mailer->aReplacements["PID"] = $oToken->participant_id;
        }
        /* Own replacements for header */
        if (empty($this->aBaseReplacements[$language])) {
            $this->aBaseReplacements[$language]["SID"] = $surveyId;
            $this->aBaseReplacements[$language]["EXPIRY"] = $oSurvey->expires;
            $this->aBaseReplacements[$language]["ADMINNAME"] = $oSurvey->oOptions->admin;
            $this->aBaseReplacements[$language]["ADMINEMAIL"] = $oSurvey->oOptions->adminemail;
            $oSurveyLanguageSettings = SurveyLanguageSetting::model()->findByPk(array('surveyls_survey_id' => $surveyId, 'surveyls_language' => $language));
            $this->aBaseReplacements[$language]["SURVEYNAME"] = $oSurveyLanguageSettings->surveyls_title;
            $this->aBaseReplacements[$language]["SURVEYDESCRIPTION"] = $oSurveyLanguageSettings->surveyls_description;
        }
        $aReplacements = array_merge(
            $this->aBaseReplacements[$language],
            $mailer->aReplacements,
            $mailer->getTokenReplacements()
        );

        /* And now the header */
        $aCustomHeaders = array();
        $XMailgunTags = trim($this->get('XMailgunTags'));
        $XMailgunTags = preg_split("/\\r\\n|\\r|\\n/", $XMailgunTags);
        if(!empty($XMailgunTags)){
            foreach($XMailgunTags as $XMailgunTag){
                $XMailgunTag = trim($this->Replacefields($XMailgunTag, $aReplacements));
                if($XMailgunTag) {
                    $mailer->addCustomHeader(
                        "X-Mailgun-Tag",
                        $XMailgunTag
                    );
                }
            }
        }
        $XMailgunVariables = $this->get('XMailgunVariables');
        $XMailgunVariables = preg_split("/\\r\\n|\\r|\\n/", $XMailgunVariables);
        if (!empty($XMailgunVariables)){
            $aVariables = array();
            foreach ($XMailgunVariables as $XMailgunVariable) {
                $XMailgunVariable = trim($this->Replacefields($XMailgunVariable, $aReplacements));
                $XMailgunVariable = explode(":",$XMailgunVariable);
                if (count($XMailgunVariable) > 1) {
                    $mailGunVariable = json_encode(array(
                        $XMailgunVariable[0] => $XMailgunVariable[1]
                    ));
                    if (strlen($mailGunVariable) > 76) {
                        $this->log("Custom header bigger than 76 characters : {$mailGunVariable}", 'warning', 'beforeTokenEmail.XMailgunVariables');
                    }
                    $mailer->addCustomHeader(
                        "X-Mailgun-Variables",
                        $mailGunVariable
                    );
                }else{
                    if(trim($XMailgunVariable[0])) {
                        $aVariables[] = $XMailgunVariable[0];
                    }
                }
            }
            if (!empty($aVariables)) {
                $mailGunVariable = json_encode($aVariables);
                if (strlen($mailGunVariable) > 76) {
                    $this->log("Custom header bigger than 76 characters : {$mailGunVariable}", 'warning', 'beforeTokenEmail.XMailgunVariables');
                }
                $mailer->addCustomHeader(
                    "X-Mailgun-Variables",
                    $mailGunVariable
                );
            }
        }
    }

    /**
    * Save settings (and fix it)
    * @see parent::saveSettings()
    */
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings','update')) {
            throw new CHttpException(403);
        }
        if (isset($settings['XMailgunTags'])) {
            $XMailgunTags = preg_split('/\r\n|\r|\n|,|;/', $settings['XMailgunTags']);
            $XMailgunTags = array_filter($XMailgunTags);
            $settings['XMailgunTags'] = implode( "\n", $XMailgunTags);
        }
        if (isset($settings['XMailgunVariables'])) {
            $XMailgunVariables = preg_split('/\r\n|\r|\n|,|;/', $settings['XMailgunVariables']);
            $XMailgunVariables = array_filter($XMailgunVariables);
            $settings['XMailgunVariables'] = implode( "\n", $XMailgunVariables);
        }
        parent::saveSettings($settings);
    }
    /**
     * repklacve expresion in string
     * @var string $string to replace
     * @var string[] $aReplacements replacements for Expression
     * @return string
     */
    private function replaceFields($string, $aReplacements) {
        return LimeExpressionManager::ProcessString($string, null, $aReplacements, 3, 1, false, false, true);
    }

    /**
    * @inheritdoc adding string, by default current event
    * @param string $message
    * @param string $level From CLogger, defaults to CLogger::LEVEL_TRACE
    * @param string $logDetail
    */
    public function log($message, $level = \CLogger::LEVEL_TRACE, $logDetail = null)
    {
        if (!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        if ($logDetail) {
            $logDetail = ".".$logDetail;
        }
        $category = get_class($this);
        \Yii::log($message, $level, 'plugin.' . $category . $logDetail);
    }
}
